# Curso-Bootstrap-Platzi

Curso de Bootstrap en Platzi.

## Introducción al curso

* 01 - Repositorio del curso
* 02 - Bootstrap 4 ¿Qué trae de nuevo esta versión
* 03 - ¿ Que es un framework frontend ?
* 04 - Nuestro Proyecto Hola Mundo de Bootstrap

## Creando el sitio web

* 01 - La grilla de Bootstrap
* 02 - Reto La grilla de Bootstrap
* 03 - El footer
* 04 - El header de nuestro sitio
* 05 - Creando un carousel de imágenes
* 06 - Agregando texto informativo del evento
* 07 - Agregando botones
* 08 - Las cards de Bootstrap 4
* 09 - Pastillas de texto
* 11 - Los formularios de Bootstrap 4
* 13 - Scrollspy Conociendo la ubicación del usuario en el header
* 14 - Agregando un modal para comprar tickets
* 15 - Un nuevo formulario para completar la compra

## Deploy a producción

* 01 - Poniendo nuestro sitio en producción
* 02 - Conclusiones del curso
